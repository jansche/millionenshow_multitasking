/*
 * FreeRTOS Kernel V10.0.1
 * Copyright (C) 2017 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://www.FreeRTOS.org
 * http://aws.amazon.com/freertos
 *
 * 1 tab == 4 spaces!
 */

#include <stdlib.h>

#include <avr/io.h>
#include <avr/interrupt.h>

#include "FreeRTOS.h"
#include "task.h"

/*-----------------------------------------------------------
 * Implementation of functions defined in portable.h for the AVR port.
 *----------------------------------------------------------*/

/* Start tasks with interrupts enables. */
#define portINITAL_SREG_STATE ((StackType_t)0x80)

/*-----------------------------------------------------------*/

/* We require the address of the pxCurrentTCB variable, but don't want to know
any details of its type. */
typedef void RTOS_TCB_t;
extern volatile RTOS_TCB_t *volatile pxCurrentTCB;

/*-----------------------------------------------------------*/

/*
 * Macro to save all the general purpose registers, the save the stack pointer
 * into the TCB. RAMP and EIND registers are not saved so RAM larger than 64
 * kBytes is not supported.
 *
 * The first thing we do is save the flags then disable interrupts.  This is to
 * guard our stack against having a context switch interrupt after we have already
 * pushed the registers onto the stack - causing the 32 registers to be on the
 * stack twice.
 *
 * r1 is set to zero as the compiler expects it to be thus, however some
 * of the math routines make use of R1.
 *
 * The interrupts will have been disabled during the call to portSAVE_CONTEXT()
 * so we need not worry about reading/writing to the stack pointer.
 */

#define portSAVE_CONTEXT()									\
	__asm__ volatile (										\
		"push	r0						\n\t"				\
		"in		r0, __SREG__			\n\t"				\
		"cli							\n\t"				\
		"push	r0						\n\t"				\
		"push	r1						\n\t"				\
		"clr	r1						\n\t"				\
		"push	r2						\n\t"				\
		"push	r3						\n\t"				\
		"push	r4						\n\t"				\
		"push	r5						\n\t"				\
		"push	r6						\n\t" 				\
		"push	r7						\n\t"				\
		"push	r8						\n\t"				\
		"push	r9						\n\t"				\
		"push	r10						\n\t"				\
		"push	r11						\n\t"				\
		"push	r12						\n\t"				\
		"push	r13						\n\t"				\
		"push	r14						\n\t"				\
		"push	r15						\n\t"				\
		"push	r16						\n\t"				\
		"push	r17						\n\t"				\
		"push	r18						\n\t"				\
		"push	r19						\n\t"				\
		"push	r20						\n\t"				\
		"push	r21						\n\t"				\
		"push	r22						\n\t"				\
		"push	r23						\n\t"				\
		"push	r24						\n\t"				\
		"push	r25						\n\t"				\
		"push	r26						\n\t"				\
		"push	r27						\n\t"				\
		"push	r28						\n\t"				\
		"push	r29						\n\t"				\
		"push	r30						\n\t"				\
		"push	r31						\n\t"				\
		"lds	r26, pxCurrentTCB		\n\t"				\
		"lds	r27, pxCurrentTCB + 1	\n\t"				\
		"in		r0, __SP_L__			\n\t"				\
		"st		x+, r0					\n\t"				\
		"in		r0, __SP_H__			\n\t"				\
		"st		x+, r0					\n\t"				\
	)

/*
 * Opposite to portSAVE_CONTEXT().  Interrupts will have been disabled during
 * the context save so we can write to the stack pointer.
 */

#define portRESTORE_CONTEXT()								\
	__asm__ volatile (										\
		"lds	r26, pxCurrentTCB		\n\t"				\
		"lds	r27, pxCurrentTCB + 1	\n\t"				\
		"ld		r28, x+					\n\t"				\
		"out	__SP_L__, r28			\n\t"				\
		"ld		r29, x+					\n\t"				\
		"out	__SP_H__, r29			\n\t"				\
		"pop	r31						\n\t"				\
		"pop	r30						\n\t"				\
		"pop	r29						\n\t"				\
		"pop	r28						\n\t"				\
		"pop	r27						\n\t"				\
		"pop	r26						\n\t"				\
		"pop	r25						\n\t"				\
		"pop	r24						\n\t"				\
		"pop	r23						\n\t"				\
		"pop	r22						\n\t"				\
		"pop	r21						\n\t"				\
		"pop	r20						\n\t"				\
		"pop	r19						\n\t"				\
		"pop	r18						\n\t"				\
		"pop	r17						\n\t"				\
		"pop	r16						\n\t"				\
		"pop	r15						\n\t"				\
		"pop	r14						\n\t"				\
		"pop	r13						\n\t"				\
		"pop	r12						\n\t"				\
		"pop	r11						\n\t"				\
		"pop	r10						\n\t"				\
		"pop	r9						\n\t"				\
		"pop	r8						\n\t"				\
		"pop	r7						\n\t"				\
		"pop	r6						\n\t"				\
		"pop	r5						\n\t"				\
		"pop	r4						\n\t"				\
		"pop	r3						\n\t"				\
		"pop	r2						\n\t"				\
		"pop	r1						\n\t"				\
		"pop	r0						\n\t"				\
		"out	__SREG__, r0			\n\t"				\
		"pop	r0						\n\t"				\
	)

/*-----------------------------------------------------------*/

/*
 * Perform hardware setup to enable ticks from timer TCB3, compare match A.
 */
static void prvSetupTimerInterrupt(void);

/*-----------------------------------------------------------*/

/*
 * See header file for description.
 */
StackType_t *pxPortInitialiseStack(StackType_t *pxTopOfStack, TaskFunction_t pxCode, void *pvParameters)
{
	/* Simulate how the stack would look after a call to vPortYield() generated by
	the compiler. */

	portPOINTER_SIZE_TYPE pxAddress;

	/* The start of the task code will be popped off the stack last, so place
	it on first. */
	pxAddress = (portPOINTER_SIZE_TYPE)pxCode;
	*pxTopOfStack = (StackType_t)(pxAddress & (portPOINTER_SIZE_TYPE)0x00ff);
	pxTopOfStack--;

	pxAddress >>= 8;
	*pxTopOfStack = (StackType_t)(pxAddress & (portPOINTER_SIZE_TYPE)0x00ff);
	pxTopOfStack--;

#if defined(__AVR_3_BYTE_PC__) && __AVR_3_BYTE_PC__
	/* A full 3-byte pointer would be required to do this properly but avr-gcc
     * has only 2-byte pointers. As 0 is stored as top byte all task routines 
	 * are forced to the bottom 128K of the flash. This can be done by using
     * the .lowtext label in the linker script. */
    *pxTopOfStack = (StackType_t)0x00;
    pxTopOfStack--;
#endif

	/* Next simulate the stack as if after a call to portSAVE_CONTEXT().
	portSAVE_CONTEXT places the flags on the stack immediately after r0
	to ensure the interrupts get disabled as soon as possible, and so ensuring
	the stack use is minimal should a context switch interrupt occur. */
	*pxTopOfStack = (StackType_t)0x00; /* R0 */
	pxTopOfStack--;
	*pxTopOfStack = portINITAL_SREG_STATE;
	pxTopOfStack--;

	/* Now the remaining registers. The compiler expects R1 to be 0. */
	*pxTopOfStack = (StackType_t)0x00; /* R1 */

	/* Leave R2 - R23 untouched */
	pxTopOfStack -= 23;

	/* Place the parameter on the stack in the expected location. */
	pxAddress = (portPOINTER_SIZE_TYPE)pvParameters;
	*pxTopOfStack = (StackType_t)(pxAddress & (portPOINTER_SIZE_TYPE)0x00ff);
	pxTopOfStack--;

	pxAddress >>= 8;
	*pxTopOfStack = (StackType_t)(pxAddress & (portPOINTER_SIZE_TYPE)0x00ff);

	/* Leave register R26 - R31 untouched */
	pxTopOfStack -= 7;

	return pxTopOfStack;
}

/*-----------------------------------------------------------*/

BaseType_t xPortStartScheduler(void)
{
	/* Setup the hardware to generate the tick. */
	prvSetupTimerInterrupt();

	/* Restore the context of the first task that is going to run. */
	portRESTORE_CONTEXT();

	/* Simulate a function call end as generated by the compiler.  We will now
	jump to the start of the task the context of which we have just restored. */
	__asm__ volatile("ret");

	/* Should not get here. */
	return pdTRUE;
}

/*-----------------------------------------------------------*/

void vPortEndScheduler(void)
{
	/* It is unlikely that the AVR port will get stopped. If required simply
	disable the tick interrupt here. */
}

/*-----------------------------------------------------------*/

/*
 * Manual context switch.  The first thing we do is save the registers so we
 * can use a naked attribute.
 */
void vPortYield(void) __attribute__((naked));
void vPortYield(void)
{
	portSAVE_CONTEXT();
	vTaskSwitchContext();
	portRESTORE_CONTEXT();
	__asm__ volatile("ret");
}

/*-----------------------------------------------------------*/

/*
 * Manual context switch callable from ISRs.  The first thing we do is save the registers so we
 * can use a naked attribute.
 */
void vPortYieldFromISR(void) __attribute__((naked));
void vPortYieldFromISR(void)
{
	portSAVE_CONTEXT();
	vTaskSwitchContext();
	portRESTORE_CONTEXT();
	__asm__ volatile("reti");
}

/*-----------------------------------------------------------*/

/*
 * Context switch function used by the tick.  This must be identical to
 * vPortYield() from the call to vTaskSwitchContext() onwards.  The only
 * difference from vPortYield() is the tick count is incremented as the
 * call comes from the tick ISR.
 */
void vPortYieldFromTick(void) __attribute__((naked));
void vPortYieldFromTick(void)
{
	portSAVE_CONTEXT();
	if (xTaskIncrementTick() != pdFALSE) {
		vTaskSwitchContext();
	}
	portRESTORE_CONTEXT();
	__asm__ volatile("reti");
}

/*-----------------------------------------------------------*/

/*
 * Setup timer TCB3 compare match A to generate a tick interrupt.
 */
static void prvSetupTimerInterrupt(void)
{
	TCB3.CCMP    = configCPU_CLOCK_HZ / configTICK_RATE_HZ - 1;
	TCB3.INTCTRL = TCB_CAPT_bm;
	TCB3.CTRLA   = TCB_ENABLE_bm;
}

/*-----------------------------------------------------------*/

#if configUSE_PREEMPTION == 1

/*
 * Tick ISR for preemptive scheduler.  We can use a naked attribute as
 * the context is saved at the start of vPortYieldFromTick().  The tick
 * count is incremented after the context is saved.
 */
ISR(TCB3_INT_vect, ISR_NAKED)
{
	/* Clear RTC interrupt flags */
	asm volatile("push r16");
	asm volatile("ldi r16, 0x1");
	asm volatile("sts 0x0AB6, r16");
	asm volatile("pop r16");

	vPortYieldFromTick();
	__asm__ volatile("reti");
}
#else

/*
 * Tick ISR for the cooperative scheduler.  All this does is increment the
 * tick count.  We don't need to switch context, this can only be done by
 * manual calls to taskYIELD();
 */
ISR(TCB3_INT_vect)
{
	/* Clear RTC interrupt flags */
	TCB3.INTFLAGS = TCB_CAPT_bm;
	xTaskIncrementTick();
}
#endif
