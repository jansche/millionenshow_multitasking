#include <avr/io.h>
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <stdio.h>
#include <stdlib.h>
#include "board.h"

SemaphoreHandle_t ErgebnisAblauf;
SemaphoreHandle_t AblaufLinksAbfrage;
SemaphoreHandle_t AblaufRechtsAbfrage;
SemaphoreHandle_t LinksAbrageLinksErst;
SemaphoreHandle_t LinksAbfrageRechtsErst;
SemaphoreHandle_t RechtsAbfrageLinksErst;
SemaphoreHandle_t RechtsAbfrageRechtsErst;
SemaphoreHandle_t Mutex;
SemaphoreHandle_t LinksErstErgebnis;
SemaphoreHandle_t RechtsErstErgebnis;

volatile int Erst = 0;
volatile int LinksButton = 5;
volatile int RechtsButton = 5;
volatile int Antwort = 0;

void Ablauf(void* pvParameters) {
	
	while (1) {
		xSemaphoreTake(ErgebnisAblauf, portMAX_DELAY);
		Antwort = rand() % (3 - 0 + 1);
		LED_PORT.OUT |=  (LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm|LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);
		vTaskDelay(2000);
		LED_PORT.OUT &= ~(LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm|LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);
		vTaskDelay(3000);
		LED_PORT.OUT |=  (LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm|LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);
		xSemaphoreGive(AblaufLinksAbfrage);
		xSemaphoreGive(AblaufRechtsAbfrage);
	}
}
		
void AbfrageLinks(void* pvParameters) {
	
	while (1) {
		xSemaphoreTake(AblaufLinksAbfrage, portMAX_DELAY);
		while(LinksButton == 5) {
			if (!(BUTTON_LOW_PORT.IN & BUTTON0_PIN_bm)) {
				LinksButton = 0;
				LED_PORT.OUT &= ~(LED0_PIN_bm);
			}
			if (!(BUTTON_LOW_PORT.IN & BUTTON1_PIN_bm)) {
				LinksButton = 1;
				LED_PORT.OUT &= ~(LED1_PIN_bm);
			}
			if (!(BUTTON_LOW_PORT.IN & BUTTON2_PIN_bm)) {
				LinksButton = 2;
				LED_PORT.OUT &= ~(LED2_PIN_bm);
			}
			if (!(BUTTON_LOW_PORT.IN & BUTTON3_PIN_bm)) {
				LinksButton = 3;
				LED_PORT.OUT &= ~(LED3_PIN_bm);
			}
		}
		xSemaphoreGive(LinksAbrageLinksErst);
		xSemaphoreGive(LinksAbfrageRechtsErst);
	}
}

void AbfrageRechts(void* pvParameters) {
	
	while (1) {
		xSemaphoreTake(AblaufRechtsAbfrage, portMAX_DELAY);
		while (RechtsButton == 5) {
			if (!(BUTTON_LOW_PORT.IN & BUTTON4_PIN_bm)) {
				RechtsButton = 0;
				LED_PORT.OUT &= ~(LED4_PIN_bm);
			}
			if (!(BUTTON_LOW_PORT.IN & BUTTON5_PIN_bm)) {
				RechtsButton = 1;
				LED_PORT.OUT &= ~(LED5_PIN_bm);
			}
			if (!(BUTTON_HIGH_PORT.IN & BUTTON6_PIN_bm)) {
				RechtsButton = 2;
				LED_PORT.OUT &= ~(LED6_PIN_bm);
			}
			if (!(BUTTON_HIGH_PORT.IN & BUTTON7_PIN_bm)) {
				RechtsButton = 3;
				LED_PORT.OUT &= ~(LED7_PIN_bm);
			}
		}
		xSemaphoreGive(RechtsAbfrageRechtsErst);
		xSemaphoreGive(RechtsAbfrageLinksErst);
	}
}

void LinksErst(void* pvParameters) {
	
	while (1) {
		xSemaphoreTake(LinksAbrageLinksErst, portMAX_DELAY);
		xSemaphoreTake(RechtsAbfrageLinksErst, portMAX_DELAY);
		xSemaphoreTake(Mutex, portMAX_DELAY);
		
		if(Erst == 0) {
			Erst = 1;
		}
		
		xSemaphoreGive(Mutex);
		xSemaphoreGive(LinksErstErgebnis);
	}
}

void RechtsErst(void* pvParameters) {
	
	while (1) {
		xSemaphoreTake(LinksAbfrageRechtsErst, portMAX_DELAY);
		xSemaphoreTake(RechtsAbfrageRechtsErst, portMAX_DELAY);
		xSemaphoreTake(Mutex, portMAX_DELAY);
		
		if(Erst == 0) {
			Erst = 2;
		}
		xSemaphoreGive(Mutex);
		xSemaphoreGive(RechtsErstErgebnis);
	}
}
		
void Ergebnis(void* pvParameters) {
	
	while (1) {		
		xSemaphoreTake(LinksErstErgebnis, portMAX_DELAY);
		xSemaphoreTake(RechtsErstErgebnis, portMAX_DELAY);
		
		vTaskDelay(500);
		LED_PORT.OUT |=  (LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm|LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);
		
		if(Erst == 1 && LinksButton == Antwort) {
			LED_PORT.OUT &= ~(LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT |=  (LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT &= ~(LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT |=  (LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT &= ~(LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT |=  (LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT &= ~(LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT |=  (LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT &= ~(LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT |=  (LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm);
		}
		else if (Erst == 2 && RechtsButton == Antwort) {
			LED_PORT.OUT &= ~(LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT |=  (LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT &= ~(LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT |=  (LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT &= ~(LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT |=  (LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT &= ~(LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT |=  (LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);
			vTaskDelay(500);
		}
		else if (Erst == 1 && RechtsButton == Antwort) {
			LED_PORT.OUT &= ~(LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT |=  (LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT &= ~(LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT |=  (LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT &= ~(LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT |=  (LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT &= ~(LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT |=  (LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);
			vTaskDelay(500);
		}
		else if (Erst == 2 && LinksButton == Antwort) {
			LED_PORT.OUT &= ~(LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT |=  (LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT &= ~(LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT |=  (LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT &= ~(LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT |=  (LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT &= ~(LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm);
			vTaskDelay(500);
			LED_PORT.OUT |=  (LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm);
			vTaskDelay(500);
		}
		
		Erst = 0;
		LinksButton = 5;
		RechtsButton = 5;
		xSemaphoreGive(ErgebnisAblauf);
	}
}


int main(void)
{
	LED_PORT.OUT = (LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm|LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);		
	LED_PORT.DIR = (LED0_PIN_bm|LED1_PIN_bm|LED2_PIN_bm|LED3_PIN_bm|LED4_PIN_bm|LED5_PIN_bm|LED6_PIN_bm|LED7_PIN_bm);
	
	BUTTON0_PINCTRL = PORT_OPC_PULLUP_gc;
	BUTTON1_PINCTRL = PORT_OPC_PULLUP_gc;
	BUTTON2_PINCTRL = PORT_OPC_PULLUP_gc;
	BUTTON3_PINCTRL = PORT_OPC_PULLUP_gc;
	BUTTON4_PINCTRL = PORT_OPC_PULLUP_gc;
	BUTTON5_PINCTRL = PORT_OPC_PULLUP_gc;
	BUTTON6_PINCTRL = PORT_OPC_PULLUP_gc;
	BUTTON7_PINCTRL = PORT_OPC_PULLUP_gc;
	
	ErgebnisAblauf = xSemaphoreCreateBinary();
	AblaufLinksAbfrage = xSemaphoreCreateBinary();
	AblaufRechtsAbfrage = xSemaphoreCreateBinary();
	LinksAbrageLinksErst = xSemaphoreCreateBinary();
	LinksAbfrageRechtsErst = xSemaphoreCreateBinary();
	RechtsAbfrageLinksErst = xSemaphoreCreateBinary();
	RechtsAbfrageRechtsErst = xSemaphoreCreateBinary();
	Mutex = xSemaphoreCreateMutex();
	LinksErstErgebnis = xSemaphoreCreateBinary();
	RechtsErstErgebnis = xSemaphoreCreateBinary();
	
	xSemaphoreGive(ErgebnisAblauf);
	xSemaphoreGive(Mutex);
	
	xTaskCreate(Ablauf, "Ablauf", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+1, NULL);
	xTaskCreate(AbfrageLinks, "AbfrageLinks", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+1, NULL);
	xTaskCreate(AbfrageRechts, "AbfrageRechts", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+1, NULL);
	xTaskCreate(LinksErst, "LinksErst", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+1, NULL);
	xTaskCreate(RechtsErst, "RechtsErst", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+1, NULL);
	xTaskCreate(Ergebnis, "Ergebnis", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY+1, NULL);
	
	vTaskStartScheduler();
}
